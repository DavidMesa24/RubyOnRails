# Receives a text, removes extra spaces and returns to the console the number of times the 'dis' text begins a word and when the 'ing' text ends a word.
# Create an 'output.txt' file with the text correction.
cont_a = 0
cont_b = 0
File.foreach('strings.txt') do |line|
  if line[0] == "."
    line[0].gsub(".", "")
  end
  cont_a += line.downcase.scan(' dis').length
  cont_a += line.downcase.scan('.dis').length
  cont_b += line.downcase.scan('ing ').length
  cont_b += line.downcase.scan('ing.').length
  cont_b += line.downcase.scan("ing\n").length
  if File.exist?('output.txt')
    File.open('output.txt', 'a') do |file|
      file.write(line.lstrip.gsub('  ', ' ').gsub('  ', ' ').gsub(' .', '.').gsub('  ', ' ').gsub("\n ", "\n").gsub("\n.", ".\n").gsub("..", ". "))
    end
  else
    File.write('output.txt', line.gsub('  ', ' ').gsub('  ', ' ').gsub(' .', '.').gsub('  ', ' ').gsub("\n ", "\n"))
  end
end
puts "Times when the word 'dis' is at the start of a word #{cont_a}"
puts "Times when the word 'ing' is at the end of a word #{cont_b}"