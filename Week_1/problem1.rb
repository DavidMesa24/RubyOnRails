# Prints a sequence of characters from 'a' to 'z'. Ex: a,b,c...zx,zy,zz

# sample data
x = 'a'..'z'
arr = []
x.to_a.each { |a| arr << a}
x.to_a.each { |a| x.to_a.each { |b| arr << (a + b) } } 
puts arr.join(',')
