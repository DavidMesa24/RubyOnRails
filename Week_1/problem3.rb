# Returns the mean of a data series. @p: data[]
def mean(data)
  cont = 0
  data.each { |numb| cont += numb }
  cont / data.length.to_f
end

# Returns the median of a data series. @p: data[]
def median(data)
  data = data.sort
  if data.length.even?
    (data[((data.length / 2) - 0.5)] + data[((data.length / 2) + 0.5)]) / 2.to_f
  else
    data[data.length / 2]
  end
end

# Returns the mode of a data series. @p: data[]
def mode(data)
  cont = 1
  mode = data[1]
  data.each do |a|
    if data.count(a) > cont
      cont = data.count(a)
      mode = a
    end
  end
  mode
end

# sample data
x = [5, 4, 2, 1, 3, 6, 7, 8, 9, 5, 4, 7, 5, 6, 2, 1, 5, 7, 8, 5, 5, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9]
puts "Data: #{x}"
puts "Data sort: #{x.sort}"
puts "Mean: #{mean(x)}"
puts "Mode: #{mode(x)}"
puts "Median: #{median(x)}"
