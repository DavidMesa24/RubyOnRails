# Prints the frequency (f) in which a data (x) is repeated. Format: x ==>> f
def to_histogram(data)
  array = data.sort
  col = []
  array.each do |a|
    unless col.include?(a)
      puts(a.to_s + ' ==>> ' + array.count(a).to_s)
      col.append(a)
    end
  end
end

# sample data

x = [3, 2, 1, 5, 4, 2, 5, 4, 1, 3, 6, 8, 9, 7, 4, 1, 5, 8, 7, 5, 7, 8, 5, 6, 4, 4, 5, 4, 5, 8, 5, 2, 4, 6, 5, 7, 8, 9,
     2, 1, 5, 4, 8, 7]
to_histogram(x)
