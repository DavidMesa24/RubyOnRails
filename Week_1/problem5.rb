#CSV 'correct_output.csv': Correct dates and Stats
#CSV 'wrong_output.csv': Wrong outputs and line
#PNG 'data.png': nice Stats Pie chart

require 'rubygems'
require 'gruff'
require 'base64'
require 'csv'
# Method that converts a date from String format to Date format.
# Exception 'e' if the date format is incorrect.

def convertirFecha(fecha, linea)
  d = Date.strptime(fecha, '%d/%m/%Y %H:%M')
rescue StandardError => e
  "Type conversion error: #{e} on line #{linea}"
end
errorescont = 0
fechas = []
errores = []
linea = 0

# It loops through the csv file and splits it by lines. row[1] is the date.
CSV.foreach('events.csv') do |row|
  linea += 1
  fecha_str = row[1]
  fecha = convertirFecha(fecha_str, linea)
  if fecha.instance_of?(Date)
    fechas.append(fecha_str)
  else
    puts fecha
    errores.append(fecha + " " + row[0])
    errorescont += 1
  end
end
fechas_ordenadas = fechas.sort
# Create the chart PIE
p = Gruff::Pie.new
p.title = 'Data summary'
p.data('Correct data', [linea - errorescont])
p.data('wrong data', [errorescont])
grafic = p.write('data.png')

# Entering the values ​​with errors in a CSV file
CSV.open('wrong_output.csv', 'ab') do |file|
  errores.each do |error|
    arr = []
    arr.append(error)
    file << arr
  end
end

arr = ["Total data: " + ("/" * linea)]
arr2 = ["Wrong data: " + ("/" * errorescont)]
arr3 = ["Error rate: #{((errorescont)*100)/linea.to_f}%"]

# Entering the values ​​without errors in a CSV file
CSV.open('correct_output.csv', 'ab') do |file|
  fechas_ordenadas.each do |fecha|
    arr = []
    arr.append(fecha)
    file << arr
  end
end
# final statistics
CSV.open('correct_output.csv', 'ab') do |file|
  arr = ["Total data: " + ("/" * linea)]
  arr2 = ["Wrong data: " + ("/" * errorescont)]
  arr3 = ["Error rate: #{((errorescont)*100)/linea.to_f}%"]
  file << arr
  file << arr2
  file << arr3
end
