Test Data:
order_s = OrderLine.find(91)
pr = Product.find(10)
client = Customer.find(10)
start_date = Date.new(2022,1,1)
end_date = Date.new(2022,12,31)

--Get a random Product------------------------------------------------------------------------------------------------------------------------------------------------------
pRandom = Product.all.sample
--Select the order of an order line-----------------------------------------------------------------------------------------------------------------------------------------
order_s.order
--Select All orders that contains an X product------------------------------------------------------------------------------------------------------------------------------
pr.orders
--Select the total of sales of X product------------------------------------------------------------------------------------------------------------------------------------
pr.order_lines { |sales, order_line| sales + order_line.total }
#Mejor así 
pr.order_lines.sum(:total)
--Select All the customers who bought a product with price greater than $60, sorted by product name (include customer, product and order information)-----------------------
Customer.select('customers.id, orders.total, order_lines.product_id, products.price, products.name').joins(:orders, :order_lines, :products).where('products.price > 60')
--Select All orders between dates X and Y-----------------------------------------------------------------------------------------------------------------------------------
Order.where(date: start_date..end_date)
--Count the total of customer who buy a product, with the amount of product ordered desc by total customer,------------------------------------------------------------------
Product.select('products.id,products.name,COUNT(customers.*) AS total_customers ').joins(:customers).group('products.id').order(total_customers: :desc)
--Select All the products a X Customer has bought ordered by date"
client.products.order('orders.date')
--Select the total amount of products a X customer bought between 2 dates---------------------------------------------------------------------------------------------------
client.order_lines.where(orders: { date: start_date..end_date }).sum('order_lines.quantiy')
--Select the id of the 3 customer that has expend more----------------------------------------------------------------------------------------------------------------------
Customer.select(:id).joins(:orders).group('customers.id').order('sum(orders.total) desc').limit(3)
--Select what is the most purchased product---------------------------------------------------------------------------------------------------------------------------------
Product.joins(:order_lines).group(:id).order('sum(order_lines.quantiy) desc').limit(1)
--Update products stock to 10 when stock is smaller than 3------------------------------------------------------------------------------------------------------------------
Product.where('stock < 3').update(stock: 10)

 TEST VALIDATION

# Create one product without name
product = Product.new({
  sku: "1234-ABC",
  description: "Some description",
  price: rand(10..100),
  stock: rand(1..25)
})
product.save!

# Update a product without price
product = Product.find(1)
product.update()

# Create user without name
user = Customer.new({
  last_name: 'Last Name',
  address: '123 Street',
  phone: '123.456.7890'
})
user.save!