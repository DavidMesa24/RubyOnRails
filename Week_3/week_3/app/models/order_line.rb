class OrderLine < ApplicationRecord
  validates :quantiy, presence: true, numericality: { only_integer: true }
  validate :stock, on: :create
  before_create :c_price, :calculate_total
  after_commit :update_stock, :calculate_order_total, on: :create
  belongs_to :order
  belongs_to :product

  def calculate_total
    self.total = quantiy * price
  end

  def c_price
    self.price = product.price
  end

  def update_stock
    product.stock -= quantiy
    product.save
  end

  def calculate_order_total
    #order.reload 
    reload_order
    sub_totals = order.order_lines.to_a.map { |ol| ol.total }
    new_total = sub_totals.reduce(:+)
    order.update(total: new_total)
  end

  def stock
    errors.add(:quantiy, "there isn't enough stock of #{product.name}") if quantiy > product.stock
  end
end
