class Product < ApplicationRecord
  validates :sku, :name, :price, presence: { message: 'must be specified for Product' }
  has_many :order_lines
  has_many :orders, through: :order_lines
  has_many :customers, through: :orders
  scope :sort_by_price, -> { unscoped.order('price') }
  scope :most_purchased, -> { unscoped.joins(:order_lines).group(:id).order('sum(order_lines.quantiy) desc').limit(1) }
  #Mejor where
  scope :name_more_two_letters, -> { where('LENGTH(name) > 99') }
  default_scope { order(:name) }
end
