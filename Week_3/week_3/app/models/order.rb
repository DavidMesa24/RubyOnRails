class Order < ApplicationRecord
  enum status: [:pending, :active, :archived]
  belongs_to :customer
  has_many :order_lines, dependent: :destroy
end
