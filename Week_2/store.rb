require_relative 'vehicles'
require_relative 'extras'

# Main class Store, have vehicles and extrass
class Store
  attr_reader :vehicles

  @@extras = Extras.new
  @@extras.import(option: rand(0..4), price: rand(100..1500))
  @@extras.import(option: rand(0..4), price: rand(100..1500))
  @@extras.import(option: rand(0..4), price: rand(100..1500))

  # search veicle. param pid = id of vehicle

  def search(pid)
    a = nil
    @vehicles.each do |veh|
      a = veh if veh.id == pid
    end
    a
  end
  # Remove vehicle. param pid = id of vehicle

  def remove(pid)
    to_delete = search(pid)
    @vehicles.delete(to_delete)
    puts "Deleted vehicle whith id: #{pid}"
  end


  # Add a vehice to vehicles Array
  def add(vehicle)
    @vehicles << vehicle
  end


  # list vehicles to be printed

  def list
    @vehicles.each do |v|
      print_detail(v, @@extras)
    end
  end

  # Print on console vehicle and extras details

  def print_detail(vehicle, _extras)
    puts "#{vehicle}
      _____________________________________________________________
        #{@@extras.list}
      _____________________________________________________________
            Total: $#{format('%.2f', (vehicle.price + (@@extras.reduce(0) do |total, extra|
                                                         total + extra.price
                                                       end || 0)))}
      "
  end

  # Start the vehicles and store. 5 of each tipe

  def start
    @vehicles = []
    Car.create_vehicles.each do |v|
      add(v)
    end

    Truck.create_vehicles.each do |v|
      add(v)
    end
  end
end
