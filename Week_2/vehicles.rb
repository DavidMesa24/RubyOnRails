require 'faker'

# Class Vehicle make 5 vehicles for each type (Car and truck)
class Vehicle
  attr_accessor :id, :number_wheels, :color, :brand, :price

  def initialize(id:, number_wheels:, color:, brand:, price:)
    @id = id
    @number_wheels = number_wheels
    @color = color
    @brand = brand
    @price = price
  end

  def self.create_vehicles
    @vehicles = []

    5.times do |_veh|
      @vehicles << new(id: Faker::Vehicle.vin, number_wheels: 4, color: Faker::Vehicle.color, brand: Faker::Vehicle.make,
                       price: Faker::Commerce.price(range: 10_000..120_000))
    end
    @vehicles
  end

  # to_s format
  def to_s
    "
        Detail:

        Id: #{id}
        FEATURES
        color: #{color}
        brand: #{brand}
        wheels: #{number_wheels}
        price: $#{price}
    "
  end
end

class Car < Vehicle
end

class Truck < Vehicle
end
