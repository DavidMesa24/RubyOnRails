require_relative 'extras'
require_relative 'vehicles'
require_relative 'store'
store = Store.new
store.start
store.search(2)
op = -1
while op.to_i != 0

  puts '
   ----------------------
  /       CAR STORE       \
 / ---------------------   \
|   Welcome to Car Store    |
#############################
|1. Get Vehicles and extras |
|---------------------------|
|2. Add a new vehicle       |
|---------------------------|
|3. Delete a vehicle        |
|---------------------------|
|0. Exit                    |
----------------------------'
  op = gets.chomp
  case op.to_i
  when 1
    store.list
  when 2
    o = 0
    puts "Select type of vehicle:
    1. Car
    2. Truck"
    o = gets.chomp
    case o.to_i
    when 1
      puts 'Id car:'
      id = gets.chomp
      if store.search(id).nil?

        puts 'Color:'
        color = gets.chomp
        puts 'Brand:'
        brand = gets.chomp
        puts 'Price:'
        price = gets.chomp
        car = Car.new(id: id, number_wheels: 4, color: color, brand: brand,
                      price: price.to_i)
        store.add(car)
        puts 'Adding vehicle'
      else
        puts 'id already exist'
      end

    when 2
      puts 'Id Truck:'
      id = gets.chomp
      if store.search(id).nil?
        puts 'Color:'
        color = gets.chomp
        puts 'Brand:'
        brand = gets.chomp
        puts 'Wheels number:'
        wheels = gets.chomp
        puts 'Price:'
        price = gets.chomp
        truck = Truck.new(id: id, number_wheels: wheels, color: color, brand: brand,
                          price: price.to_i)
        store.add(truck)
        puts 'Adding vehicle'
      else
        puts 'id already exist'
      end

      sleep(1)
    end
  when 3
    puts 'Enter the ID of the vehicle you want to delete'
    id = gets.chomp
    if store.search(id).nil?
      puts 'vehicle does not exist'
    else
      puts "Are you sure you want to delete the vehicle with id #{id}? (S/N)"
      el = gets.chomp.downcase
      case el
      when 's'
        store.remove(id)
      when 'n'
        puts 'Canceled action'
      else
        puts 'Invalid option'
      end
    end
  end
end
